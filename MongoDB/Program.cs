﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.DAL;
using MongoDB.Models;
using MongoDB.Service;

namespace MongoDB
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IMongoDAL, MongoDAL>()
                .AddSingleton<IQuoteService, QuoteService>()
                .BuildServiceProvider();

            var quote = serviceProvider.GetService<IQuoteService>();
            var result = quote.GetCollection();

        }
    }
}
