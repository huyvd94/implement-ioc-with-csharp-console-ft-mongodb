﻿using MongoDB.Bson;
using System.Collections.Generic;

namespace MongoDB.DAL
{
    public interface IMongoDAL
    {
        void DeleteDocument<T>(string Collection, ObjectId id);
        List<T> GetCollection<T>(string Collection);
        T GetDocumentById<T>(string Collection, ObjectId Id);
        bool InsertDocument<T>(string Collection, T Record);
        void UpsertDocument<T>(string Collection, ObjectId id, T document);
    }
}