﻿using MongoDB.Bson;
using MongoDB.DAL;
using MongoDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDB.Service
{
    public class QuoteService : IQuoteService
    {
        private const string Collection = "quote";
        private readonly IMongoDAL _mongo;
        public QuoteService(IMongoDAL mongo)
        {
            _mongo = mongo;
        }

        public IList<QuoteModel> GetCollection()
        {
            return _mongo.GetCollection<QuoteModel>(Collection);
        }
        public bool InsertDocument(QuoteModel record)
        {
            return _mongo.InsertDocument(Collection, record);
        }


        public void UpsertDocument(ObjectId id, QuoteModel document)
        {
            _mongo.UpsertDocument(Collection, id, document);
        }
        public void DeleteDocument(ObjectId id)
        {
            _mongo.DeleteDocument<QuoteModel>(Collection, id);
        }
    }
}
