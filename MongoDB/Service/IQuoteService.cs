﻿using MongoDB.Bson;
using MongoDB.Models;
using System.Collections.Generic;

namespace MongoDB.Service
{
    public interface IQuoteService
    {
        IList<QuoteModel> GetCollection();
        bool InsertDocument(QuoteModel record);
        void UpsertDocument(ObjectId id, QuoteModel document);
        void DeleteDocument(ObjectId id);
    }
}